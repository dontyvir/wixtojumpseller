#!/usr/bin/env node

const yargs = require("yargs");
const Papa = require("papaparse");
const fs = require("fs");
const util = require("util");
const readFile = util.promisify(fs.readFile);
const convertDocument = require("./src/convertDocument.module");

async function parse(fileName){
  const inputBuffer = await readFile(fileName);
  const inputCsvString = inputBuffer.toString();
  const results = Papa.parse(inputCsvString,{delimiter:",",quoteChar: '"',header:true});
  return results;
}
async function writeOutput(data, outputFile){
  let csvData = Papa.unparse(data);
  console.log(csvData);
}

async function main(){
    try {
        // Greeting + params

        const options = yargs
            .usage("Usage: -i <input file> -o <output file>")
            .option("i", {alias: "input", describe: "input csv file", type: "string", demandOption: true})
            .option("o", {alias: "output", describe: "output csv file", type: "string", demandOption: false})
            .argv;
        if(options.output) {
            console.log("Wix to jumpseller product csv converter");
            console.log(`Converting, ${options.input} to ${options.output}`);
        }
        // Conversion
        let inputData = await parse(options.input);
        let covertedData = convertDocument(inputData);
        await writeOutput(covertedData, options.output);

    } catch(e){
        console.error(e);
        return 1;
    }
}

main();