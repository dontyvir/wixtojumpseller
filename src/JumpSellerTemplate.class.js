"use strict";

class JumpSellerTemplate {
    constructor() {
            this.Permalink= '';
            this.Name= '';
            this.Description= '';
            this['Meta Title']= '';
            this['Meta Description']= '';
            this.Width= '';
            this.Length= '';
            this.Height= '';
            this.Brand= '';
            this.Categories= '';
            this.Images= '';
            this.Digital= 'NO';
            this.Featured= 'NO';
            this.Status= 'available';
            this.SKU= '';
            this.Weight= '';
            this.Stock= '';
            this['Stock Unlimited']= 'NO';
            this.Price= '';
            this['Custom Field 1 Label']= '';
            this['Custom Field 1 Value']= '';
            this['Custom Field 2 Label']= '';
            this['Custom Field 2 Value']= ''
            this['Variant Image']= '';
            this['Variant 1 Option Name']= '';
            this['Variant 1 Option Type']= '';
            this['Variant 1 Option Value']= '';
            this['Variant 2 Option Name']= '';
            this['Variant 2 Option Type']= '';
            this['Variant 2 Option Value']= '';

    }
}
module.exports = JumpSellerTemplate;
