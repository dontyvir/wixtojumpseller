"use strict";

const JumpSellerTemplate = require("./JumpSellerTemplate.class");

function convertRow(inputRows, i){
    let wixRow = inputRows[i];
    const jumpSellerRow = new JumpSellerTemplate();

    // 1 to 1 Mapping
    jumpSellerRow.Name = wixRow.name;
    jumpSellerRow.Description = wixRow.description;
    jumpSellerRow.Stock = wixRow.inventory;
    jumpSellerRow.Price = wixRow.price;
    jumpSellerRow.SKU = wixRow.sku;
    jumpSellerRow.Weight = wixRow.weight;

    // colletion to Categories
    const collection = wixRow.collection || "";
    const categories = collection.replace(/;/g,",");
    jumpSellerRow.Categories = categories;

    // Images
    const productImageUrl = wixRow.productImageUrl || "";
    const images = productImageUrl.split(";");
    let jumpSellerImages = "";
    for(let i=0;i<images.length;i++){
        jumpSellerImages += `https://static.wixstatic.com/media/${images[i]}`;
        if(i<images.length-1)
            jumpSellerImages += ",";
    }
    jumpSellerRow.Images = jumpSellerImages;

    // Variants
    const prodOptions = [];
    for(let op=0;op<6;op++){
        const productOptionName = wixRow[`productOptionName${op+1}`] || "";
        if(!productOptionName) continue;
        const optionDesc = wixRow[`productOptionDescription${op+1}`] || "";
        // delete wix color codes
        optionDesc.replace(/#.{6}:/g,""); // TODO: check
        const optVariants = optionDesc.split(";");
        prodOptions.push({
            name: productOptionName,
            values: optVariants
        });
    }

    const variantRows = [];

    // NOTE: considering only first variant Category
    if(prodOptions.length > 0){
        for(let v=0;v<prodOptions[0].values.length;v++){ // variants from first Category
            let variant = prodOptions[0].values[v].replace(/#.{6}:/g,"");
            let j=i+1;
            while(inputRows[j].fieldType === 'Variant'){
                const variantRow = inputRows[j];
                const rowDescription = variantRow.productOptionDescription1.replace(/#.{6}:/g,"");
                if(rowDescription === variant){

                    let outVariantRow;
                    // If, first variant, use product row instead of new variant row
                    if(v===0) outVariantRow = jumpSellerRow;
                    else outVariantRow = new JumpSellerTemplate();

                    outVariantRow.Stock = variantRow.inventory;
                    outVariantRow.Price = jumpSellerRow.Price;
                    outVariantRow['Variant 1 Option Name'] = prodOptions[0].name;
                    outVariantRow['Variant 1 Option Type'] = 'option';
                    outVariantRow['Variant 1 Option Value'] = variant;
                    outVariantRow['Variant Image'] = `https://static.wixstatic.com/media/${images[v]}`
                    variantRows.push(outVariantRow);
                }
                j++;
            }
        }
    }
    let jumpSellerRows;
    if(variantRows.length > 0) jumpSellerRows = variantRows;
    else jumpSellerRows = Array(jumpSellerRow);
    return jumpSellerRows;
}
function convertDocument(inputData){
    const outPutData = [];
    const inputRows = inputData.data;

    for(let i=0;i<inputRows.length;i++){

        // let convertRow manage variant rows
        if(inputRows[i].fieldType === 'Variant') continue;

        const outputRows = convertRow(inputRows, i);
        outPutData.push(...outputRows);
    }

    return outPutData;
}

module.exports = convertDocument;